use std::vec::Vec;
use std::fmt;

use crate::ehash::alias::{Hash, Value, BITS_PER_HASH, BUCKET_CAPACITY};
use crate::ehash::bucket::Bucket;

/// [`ExtHash`] exceptions.
#[derive(Debug, Copy, Clone)]
pub enum ExtHashErr {
    /// The [`ExtHash`] has reached its max depth.
    MaxDepth,
    /// A `Bucket` in [`ExtHash`] has reached its max local depth.
    MaxLocalDepth,
    /// The requested [`Bucket`] does not exist.
    NoBucketFound
}

/// Extendible hashing table.
pub struct ExtHash {
    depth: usize,
    buckets: Vec<Box<Bucket>>,
    directory: Vec<usize>,
}

// ExtHash methods
impl ExtHash { 
    /// Constructs a new, empty `ExtHash` with specified depth. 
    pub fn new(depth: usize) -> Self {
        let size = 2usize.pow(depth as u32);
        let mut ehash = Self {
            depth,
            buckets: Vec::new(),
            directory: Vec::with_capacity(size),
        };

        // initial bucket has depth of 0 since every hash will go into it
        let id = ehash.create_bucket(0);

        // every initial directory index points to the only bucket
        for _ in 0..size {
           ehash.directory.push(id); 
        }

        return ehash;
    }

    /// Inserts a hash into the table.
    pub fn insert(&mut self, h: Hash) -> Result<(), ExtHashErr> {
        let id = self.get_bucket_id(h);
        // if the insertion failed, run the failed_insert method
        match self.buckets[id].insert(h) {
            Ok(_) => Ok(()),
            Err(_) => self.failed_insert(h)
        }
    }

    /// Inserts multiple hashes from a `Vec<Hash>` into the table.
    pub fn insert_multiple(&mut self, hashes: Vec<Hash>) {
        use ExtHashErr as EHErr;
        // print out any errors from any resulting insertions
        for hash in hashes {
            match self.insert(hash) {
                Ok(_) => (),
                Err(code) => match code {
                EHErr::MaxDepth => print!("Failed to insert {}, max local depth reached", hash),
                EHErr::MaxLocalDepth => print!("Failed to insert {}, max depth reached", hash),
                EHErr::NoBucketFound => print!("Failed to insert {}, no bucket found", hash)
                }
            }
        }
    }
    
    /// Returns the hash stored at the place specified by the given hash or 
    /// [`None`] if it does not exist.
    ///
    /// Since only hashes and no actual values are being stored in this
    /// implementation, the given hash and the returned value should always be
    /// the same.
    pub fn get(&self, h: Hash) -> Option<Value> { 
        self.buckets[self.get_bucket_id(h)].get(h)
    }
    
    /// Returns the total number of hashes stored in the table. 
    pub fn size(&self) -> usize {
        let mut size = 0;

        for bucket in &self.buckets {
            size += bucket.size();
        }

        return size;
    } 

    /// Gets the id of the bucket for the given hash from the directory.
    fn get_bucket_id(&self, h: Hash) -> usize { 
        let dir_i = self.get_dir_index(h);
        return self.directory[dir_i];
    }

    /// Gets the directory index for the given hash, using the first
    /// `depth` number of bits as the index.
    fn get_dir_index(&self, h: Hash) -> usize {
        h >> (BITS_PER_HASH-self.depth)
    }

    /// Creates a new empty [`Bucket`] with the given depth and returns its id.
    fn create_bucket(&mut self, depth: usize) -> usize {
        self.buckets.push(Box::new(Bucket::new(depth)));
        return self.buckets.len()-1;
    }

    /// Called if a hash failed to insert. This will call `increase_depth()`
    /// and create new buckets as necessary.
    fn failed_insert(&mut self, h: Hash) -> Result<(), ExtHashErr> {
        let old_id = self.get_bucket_id(h);
        let old = &mut self.buckets[old_id]; // original
        let mut hashes = old.get_hashes();
        old.clear();

        // increase local depth
        let local_depth = old.local_depth() + 1;
        if old.set_depth(local_depth).is_err() {
            // if the max local depth was reached, abort bucket splitting
            self.insert_multiple(hashes);
            return Err(ExtHashErr::MaxLocalDepth);
        }

        if local_depth > self.depth {
            if self.increase_depth().is_err() {
                // if max directory depth was reached, abort bucket splitting
                self.insert_multiple(hashes);
                return Err(ExtHashErr::MaxDepth);
            }
        }

        // making new bucket and inserting into directory
        let new_id = self.create_bucket(local_depth);
        if local_depth == self.depth {
            // dont have to worry about updating more than one directory ref
            let dir_index = self.get_dir_index(h);
            self.directory[dir_index] = new_id;
        } else {
            // number of directory indices that the new bucket covers
            let depth_range = 2usize.pow((self.depth - local_depth) as u32);
            let start = match self.find_first_reference(old_id) {
                // new bucket covers the upper half of the old range
                Some(start) => start+depth_range,
                // if the old bucket somehow disappeared from the directory
                None => return Err(ExtHashErr::NoBucketFound)
            };
            // update the old directory refs
            for i in start..start+depth_range {
                self.directory[i] = new_id;
            }
        }

        hashes.push(h); // new hash needs to be reinsterted
        self.insert_multiple(hashes); // reinsert all hashes from old bucket
        return Ok(());
    }

    /// This will double the size of the directory, increase the depth by 1, as
    /// well as doubling the number of each bucket reference.
    fn increase_depth(&mut self) -> Result<(), ExtHashErr> {
        // checking new depth
        let d = self.depth+1;
        if d > BITS_PER_HASH {
            return Err(ExtHashErr::MaxDepth);
        }
        self.depth = d;

        // creating new directory
        let capacity = 2usize.pow(self.depth as u32);
        let mut new_dir = Vec::with_capacity(capacity); 

        // doubling each bucket reference
        for entry in &self.directory {
            new_dir.push(*entry);
            new_dir.push(*entry);
        }
        self.directory = new_dir;

        return Ok(());
    }

    /// Returns the directory utilization as the total number of buckets over
    /// the total number of directory entries.
    fn dir_util(&self) -> f32 {
        self.buckets_size() as f32 / self.directory_size() as f32
    }

    /// Returns the utilization of buckets as the total number of entries over the
    /// total capacity of the existing buckets.
    fn bucket_util(&self) -> f32 {
        self.size() as f32 / (self.buckets_size()*BUCKET_CAPACITY) as f32
    }

    /// Returns the number of entries in the directory. 
    fn directory_size(&self) -> usize {
        self.directory.len()
    }

    /// Returns the number of buckets.
    fn buckets_size(&self) -> usize {
        self.buckets.len()
    }
 
    /// Returns the first directory index with a reference to the given bucket.
    /// Will return [`None`] if the bucket isn't referenced in the directory.
    fn find_first_reference(&self, bucket: usize) -> Option<usize> {
        for i in 0..self.directory.len() {
            if self.directory[i] == bucket {
                return Some(i); 
            }
        }
        return None;
    }
}

// Display trait implementation
impl fmt::Display for ExtHash {
    /// Prints the current directory utilization and utilization of buckets
    /// separated by a comma.
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}, {}", self.dir_util(), self.bucket_util())
    }
}

// Default trait implementation
impl Default for ExtHash {
    /// Returns an `ExtHash` with a depth of 1.
    fn default() -> Self
    {
        Self::new(1)
    }
}

