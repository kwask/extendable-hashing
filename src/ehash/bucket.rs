use std::fmt;
use crate::ehash::alias::{Hash, Value, KeyValue, BITS_PER_HASH, BUCKET_CAPACITY};

pub type BucketValues = [Option<KeyValue>; BUCKET_CAPACITY];

/// [`Bucket`] exceptions. 
#[derive(Debug, Copy, Clone)]
pub enum BucketErr {
    /// The [`Bucket`] is full.
    Full,
    /// The [`Bucket`] has reached its max local depth
    MaxDepth
}

/// Data bucket that stores key-value pairs. 
#[derive(Copy, Clone)]
pub struct Bucket {
    local_depth: usize,
    values: BucketValues,
    size: usize,
}

// Bucket methods
impl Bucket {
    /// Returns a new empty `Bucket` with the given local_depth. The capacity
    /// for all buckets is 40 elements.
    pub fn new(local_depth: usize) -> Self {
        let values = [None; BUCKET_CAPACITY];
        Bucket {
            local_depth,
            values,
            size: 0,
        }
    }

    /// Inserts the given hash into the bucket, returning an error if anything
    /// went wrong.
    pub fn insert(&mut self, h: Hash) -> Result<(), BucketErr> {
        // get the index if there was no error finding one
        let i = self.index_from_hash(h)?;
        // store the hash as both key and value for the sake of this project
        self.values[i] = Some((h, h));
        self.size += 1;

        return Ok(());
    }
 
    /// Returns the corresponding value for the given hash, or [`None`] if
    /// none exists.
    pub fn get(&self, h: Hash) -> Option<Value> {
        let i = match self.index_from_hash(h) {
            Ok(i) => i,
            Err(_) => return None
        };
        return self.get_value(i);
    }

    /// Returns all of the hashes stored in the bucket. 
    pub fn get_hashes(&self) -> Vec<usize> {
        self.values.iter().filter(|e| // filtering out all None values 
            {
                return match e {
                    Some(_) => true,
                    None    => false
                };
            }).map(|e| // mapping Some(KeyValue) into just keys
            {
                if let Some((h, _)) = e {
                    return *h;
                } 
                return 0;
            }).collect::<Vec<usize>>() // return the collection
    }
    
    /// Gets a valid index for the given hash, using open addressing to
    /// handle collisions. Either an empty index or the current index for
    /// the given hash is returned.
    ///
    /// If no valid index is available, an error is returned.
    fn index_from_hash(&self, h: Hash) -> Result<usize, BucketErr> {
        let max_size = self.capacity();
        let value = Some(h);

        // simple linear probing if theres a collision
        for j in 0..max_size {
            let k = (h+j)%max_size;
            if self.index_empty(k) || value == self.get_hash(k) {
                return Ok(k); 
            }
        }

        return Err(BucketErr::Full);
    }

    /// Returns true if the given index is empty, false otherwise.
    fn index_empty(&self, i: usize) -> bool {
        match self.values[i] {
            Some(_) => false,
            None => true
        }
    }

    /// Returns the utilization of the bucket as the size over the capacity.
    pub fn utilization(&self) -> f32 {
        (self.size() as f32)*(1.0/self.capacity() as f32)
    }

    /// Sets the depth of the bucket, returning an error if it was
    /// unsuccessful.
    pub fn set_depth(&mut self, ld: usize) -> Result<usize, BucketErr>{
        if ld > BITS_PER_HASH {
            return Err(BucketErr::MaxDepth);
        }

        self.local_depth = ld;

        return Ok(self.local_depth);
    }

    /// Returns the local depth of the bucket.
    pub fn local_depth(&self) -> usize {
        self.local_depth
    }

    /// Returns the capacity of the bucket.
    pub fn capacity(&self) -> usize {
        self.values.len()
    }

    /// Returns the size of the bucket, which is the number of entries stored. 
    pub fn size(&self) -> usize {
        self.size
    }
    
    /// Returns true if the bucket is at capacity, false otherwise.
    pub fn is_full(&self) -> bool {
        self.size() >= self.capacity()
    }

    /// Clears out the bucket, making it empty.
    pub fn clear(&mut self) {
        self.values = [None; BUCKET_CAPACITY];
        self.size = 0;
    }

    /// Gets the hash at the given index, or [`None`] if none exists.
    fn get_hash(&self, i: usize) -> Option<Hash> {
        match self.values[i] {
            Some((hash, _)) => Some(hash),
            None => None
        }
    }

    /// Gets the value at the given index, or [`None`] if none exists.
    fn get_value(&self, i: usize) -> Option<Value> {
        match self.values[i] {
            Some((_, value)) => Some(value),
            None => None
        }
    }
}

// Display trait implementation for Bucket
impl fmt::Display for Bucket {
    /// Writes the local depth and utilization to the formatter.
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut output_str = String::new();
        if self.values.len() > 0 {
            output_str.push_str(&format!("{},{}",
                                         self.local_depth,
                                         self.utilization()));
        } else {
            output_str.push_str("Empty");
        }

        return write!(f, "{}", output_str);
    }
}

