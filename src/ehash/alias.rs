pub type Hash = usize;
/// Values are just the given hash, no other data is being stored.
pub type Value = usize;
pub type KeyValue = (Hash, Value);

pub const BUCKET_CAPACITY: usize = 40;
pub const BITS_PER_HASH: usize = std::mem::size_of::<Hash>()*8;

