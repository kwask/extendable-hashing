/** ======================================================================
 * author:         Nick Hirzel
 * email:          hirzel.2@wright.edu
 * date:           December 1, 2020 
 * course:         CS4700 Intro to DB Systems 
 *
 * instructor:     Dr. Soon M. Chung 
 * assignment:     final project 
 *
 * file:           ehash.rs
 ** =================================================================== */

use rand::Rng;
use std::path::Path;
use std::fs::File;
use std::io::prelude::*;
use chrono::prelude::Local;

pub mod ehash;
use ehash::{ExtHash, ExtHashErr};

fn main() {
    let hash_count = 10_000;
    let sample_rate = 4;

    // our extendable hashing table
    let mut db = ExtHash::default();

    let mut output_str = String::new();
    let mut rng = rand::thread_rng();
	let mut hashes = Vec::with_capacity(hash_count); 
	for i in 0..hash_count { // inserting hashes into table
        let hash: usize = rng.gen();
        hashes.push(hash);
        
        use ExtHashErr as EHErr;
        match db.insert(hash) {
            Ok(_) => (),
            Err(code) => match code {
                EHErr::MaxDepth => print!("Failed to insert {}, max local depth reached", hash),
                EHErr::MaxLocalDepth => print!("Failed to insert {}, max depth reached", hash),
                EHErr::NoBucketFound => print!("Failed to insert {}, no bucket found", hash)
            }
        }

        if i%sample_rate == 0 {
			// will print "hash_count,dir_util,bucket_util"
            output_str.push_str(&format!("{},{}\n", i, db));
        }
    }
    
    // final results
    output_str.push_str(&format!("{},{}\n", hash_count-1, db));

    // writing to output file
    let datetime = Local::now().format("%Y%m%d%H%M%S").to_string();
    let filename = format!("results_{}.csv", datetime);
    let output_path = Path::new(&filename);
    let mut output_file = match File::create(&output_path) {
        Err(why) => panic!("couldn't create {}: {}",
                            output_path.display(),
                            why),
        Ok(file) => file,
    };
    if output_file.write_all(output_str.as_bytes()).is_err() {
        print!("failed to write results file");
    }
}
