% rename the file you want to plot to results.csv
data = csvread("results.csv");
x = data(:, 1);
dir = data(:, 2);
bucket = data(:, 3);

figure
hold on
title("Bucket and Directory Utilization");
xlabel("hash count");
ylabel("utilization");
plot(x, dir, x, bucket);
legend("directory", "bucket");
saveas(gcf, "plot.png");
